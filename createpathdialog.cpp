﻿#include "createpathdialog.h"
#include "ui_createpathdialog.h"
#include <QStandardItemModel>
#include <QtConcurrent/QtConcurrent>
#include <ntlx/core/getserverlist.h>

CreatePathDialog::CreatePathDialog(QWidget *parent)
  : QDialog(parent)
  , ui_(new Ui::CreatePathDialog)
{
  ui_->setupUi(this);

  // リストビューにモデルを追加
  model_ = new QStandardItemModel(this);
  ui_->comboBox->setModel(model_);

  connect(ui_->buttonGroup, SIGNAL(buttonClicked(QAbstractButton*))
          , this, SLOT(changeLocalOrServer(QAbstractButton*))
          );
}

CreatePathDialog::~CreatePathDialog()
{
  delete ui_;
}

QString CreatePathDialog::title() const
{
  return isLocal() ? tr("Local") : ui_->comboBox->currentText();
}

QVariant CreatePathDialog::pathData() const
{
  PathData data;
  data.setServer(isLocal() ? "" : ui_->comboBox->currentText());
  data.setPath(ui_->initialPathLineEdit->text());
  return QVariant::fromValue<PathData>(data);
}

bool CreatePathDialog::isLocal() const
{
  int curId = ui_->buttonGroup->checkedId();
  int localId = ui_->buttonGroup->id(ui_->localButton);
  return curId == localId;
}

void CreatePathDialog::updateServerList()
{
  QtConcurrent::run([this]
  {
    // Notesスレッド初期化
    ntlx::ApiResult result = NotesInitThread();
    if (result.hasError())
      return;

    QList<ntlx::DName> list;
    ntlx::GetServerList()(list);
    if (list.isEmpty())
      return;

    model_->removeRows(0, model_->rowCount());

    // サーバリストをモデルに反映
    for (auto it = list.constBegin(); it != list.constEnd(); ++it)
    {
      QString serverName = it->abbreviated().toQString();
      QStandardItem* item = new QStandardItem(serverName);
      item->setEditable(false);
      model_->appendRow(item);
    }

    // Notesスレッドの終了
    NotesTermThread();
  });
}

void CreatePathDialog::changeLocalOrServer(QAbstractButton* button)
{
  if (ui_->buttonGroup->id(button) == ui_->buttonGroup->id(ui_->localButton))
    ui_->comboBox->setEnabled(false);
  else
    ui_->comboBox->setEnabled(true);
}

﻿#ifndef PATHITEM_H
#define PATHITEM_H

#include "nsf_notes_global.h"
#include <nsfinder/baseitem.h>

class NSF_NOTESSHARED_EXPORT PathItem
    : public BaseItem
{
public:
  PathItem(const QString& text);
  PathItem(const QIcon& icon, const QString& text);

  void refreshChildren(FinderModel& model) override;
};

#endif // PATHITEM_H

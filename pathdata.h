﻿#ifndef PATHDATA_H
#define PATHDATA_H

#include <QString>

class PathData
{
public:
  PathData()
    : server_()
    , path_()
  {
  }

  PathData(const PathData& other)
    : server_(other.server_)
    , path_(other.path_)
  {
  }

  PathData& operator =(const PathData& other)
  {
    if (this != &other)
    {
      server_ = other.server_;
      path_ = other.path_;
    }
    return *this;
  }

  QString server() const { return server_; }
  QString path() const { return path_; }
  void setServer(const QString& server) { server_ = server; }
  void setPath(const QString& path) { path_ = path; }

private:
  QString server_;
  QString path_;
};

#endif // PATHDATA_H

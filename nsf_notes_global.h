#ifndef NSF_NOTES_GLOBAL_H
#define NSF_NOTES_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(NSF_NOTES_LIBRARY)
#  define NSF_NOTESSHARED_EXPORT Q_DECL_EXPORT
#else
#  define NSF_NOTESSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // NSF_NOTES_GLOBAL_H

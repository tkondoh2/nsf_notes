﻿#ifndef NOTESPLUGIN_H
#define NOTESPLUGIN_H

#include "nsf_notes_global.h"
#include "nsfinder/plugininterface.h"

class NSF_NOTESSHARED_EXPORT NotesPlugin
    : public QObject, public PluginInterface
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "com.chiburu.NSFinder.PluginInterface" FILE "notesplugin.json")
  Q_INTERFACES(PluginInterface)

public:
  NotesPlugin();
  ~NotesPlugin();

  QList<QAction*> createActions(QObject* parent) override;
  void run(QObject* sender, FinderView* view) override;

private:
  QAction* action_;
};

#endif // NOTESPLUGIN_H

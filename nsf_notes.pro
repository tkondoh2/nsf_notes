#-------------------------------------------------
#
# Project created by QtCreator 2018-02-16T09:54:22
#
#-------------------------------------------------

QT       += widgets

TARGET = nsf_notes
TEMPLATE = lib
CONFIG += plugin

DEFINES += NSF_NOTES_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#
# Notes API用マクロ定義
#
win32 {
    DEFINES += W W32 NT
    contains(QMAKE_TARGET.arch, x86_64) {
        DEFINES += W64 ND64 _AMD64_
    }
    else {
        CONFIG(debug, release|debug) {
            DESTDIR = ../build-nsfinder-Desktop_Qt_5_9_3_MSVC2015_32bit-Debug/plugins
        }
        else {
            DESTDIR = ../build-nsfinder-Desktop_Qt_5_9_3_MSVC2015_32bit-Release/plugins
        }
    }
}
else:macx {
    # MACのDWORDをunsigned int(4バイト)にする
    # DEFINES += MAC
    DEFINES += MAC LONGIS64BIT
    CONFIG(debug, release|debug) {
        DESTDIR = ../build-nsfinder-Desktop_Qt_5_9_3_clang_64bit-Debug/plugins
    }
    else {
        DESTDIR = ../build-nsfinder-Desktop_Qt_5_9_3_clang_64bit-Release/plugins
    }
}
else:unix {
    DEFINES += UNIX LINUX W32
    QMAKE_CXXFLAGS += -std=c++0x
    target.path = /usr/lib
    INSTALLS += target
}

SOURCES += \
        notesplugin.cpp \
    createpathdialog.cpp \
    pathitem.cpp

HEADERS += \
        notesplugin.h \
        nsf_notes_global.h \
    createpathdialog.h \
    pathitem.h \
    pathdata.h

LIBS += -lnotes -lntlx_core

FORMS += \
    createpathdialog.ui

DISTFILES += \
    notesplugin.json

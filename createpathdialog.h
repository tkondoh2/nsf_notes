﻿#ifndef CREATEPATHDIALOG_H
#define CREATEPATHDIALOG_H

#include <QDialog>
#include <QAbstractButton>
#include "pathdata.h"

namespace Ui {
class CreatePathDialog;
}

class QStandardItemModel;

class CreatePathDialog
    : public QDialog
{
  Q_OBJECT

public:
  explicit CreatePathDialog(QWidget *parent = 0);
  ~CreatePathDialog();

  QString title() const;
  QVariant pathData() const;
  bool isLocal() const;

  void updateServerList();

public slots:
  void changeLocalOrServer(QAbstractButton* button);

private:
  Ui::CreatePathDialog* ui_;
  QStandardItemModel* model_;
};

Q_DECLARE_METATYPE(PathData)

#endif // CREATEPATHDIALOG_H

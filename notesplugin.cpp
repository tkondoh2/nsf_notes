﻿#include "notesplugin.h"
#include "createpathdialog.h"
#include "pathitem.h"
#include <QStandardItemModel>

NotesPlugin::NotesPlugin()
{
}

NotesPlugin::~NotesPlugin()
{
}

QList<QAction*> NotesPlugin::createActions(QObject* parent)
{
  action_ = new QAction(tr("New Server..."), parent);
  return QList<QAction*>() << action_;
}

void NotesPlugin::run(QObject* sender, FinderView* view)
{
  if (action_ != qobject_cast<QAction*>(sender))
    return;

  CreatePathDialog dialog;
  dialog.updateServerList();
  if (dialog.exec())
  {
    PathItem* item = new PathItem(dialog.title());
    item->setData(dialog.pathData());
    if (QStandardItemModel* model = qobject_cast<QStandardItemModel*>(view->model()))
      model->appendRow(item);
  }
}
